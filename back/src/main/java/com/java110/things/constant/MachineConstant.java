package com.java110.things.constant;

/**
 * @ClassName MachineConstant
 * @Description TODO 设备门禁常量类
 * @Author wuxw
 * @Date 2020/5/15 20:35
 * @Version 1.0
 * add by wuxw 2020/5/15
 **/
public class MachineConstant {

    // 门禁
    public static  final String MACHINE_TYPE_ACCESS_CONTROL = "9998";

    // 道闸
    public static  final String MACHINE_TYPE_BARRIER_GATE = "9999";

    // 考勤机
    public static  final String MACHINE_TYPE_ATTENDANCE = "9997";

    // 摄像头
    public static  final String MACHINE_TYPE_CAMERA = "9998";
}
